# ProR adaptor

This is a repository containing the Lyo Toolchain modelling project and the generated Java webapp Maven project.

## Getting started

1. ~~Install TRS maven modules from source (https://gitlab.com/assume/offis-svn).~~ No longer needed: latest jars are in S3 repo.
1. Build this project via maven (`mvn clean verify`)
1. Look at the [Javadoc](http://assume.gitlab.io/adaptor-pror/project-reports.html)
1. Install Lyo Toolchain if you need to modify the model behind the generated code (in order to regenerate it again).

## License

> Copyright 2023 KTH Royal Institute of Technology
> 
> Licensed under the EUPL-1.2-or-later, with extension of article 5 (compatibility clause) to any licence for distributing derivative works that have been produced by the normal use of the Work as a library.
>
> SPDX-License-Identifier: EUPL-1.2

This project uses code from the Eclipse Lyo project dual-licensed under `EPL-1.0 OR BSD-3-Clause`. We chose to use that code under the terms of `EPL-1.0`.

> Copyright (c) 2012 IBM Corporation and Contributors to the Eclipse Foundation.
> 
> All rights reserved. This program and the accompanying materials
> are made available under the terms of the Eclipse Public License v1.0
> and Eclipse Distribution License v. 1.0 which accompanies this distribution.
> 
> The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
> and the Eclipse Distribution License is available at
> http://www.eclipse.org/org/documents/edl-v10.php.
> 
> SPDX-License-Identifier: EPL-1.0 OR BSD-3-Clause