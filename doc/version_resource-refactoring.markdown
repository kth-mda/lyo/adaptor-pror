# Version Resource refactoring notes

After removing the VR class, the following things were changed:

* `RequirementAndRequirementCollectionAndVersionResourceService` was renamed
* `ProRConstants.PATH_VERSIONRESOURCE` was deleted
* `RESOURCE_CLASSES.add(VersionResource.class);` was deleted
* `RESOURCE_SHAPE_PATH_TO_RESOURCE_CLASS_MAP.put(ProRConstants.PATH_VERSIONRESOURCE, VersionResource.class);` was deleted
*

The removed constants:

    public static String VERSIONRESOURCE = "VersionResource";
    public static String PATH_VERSIONRESOURCE = "versionResource";
    public static String TYPE_VERSIONRESOURCE = CFGM_NAMSPACE + "VersionResource";

Removed ProRManager methods:

    public static List<VersionResource> queryVersionResources(HttpServletRequest httpServletRequest, final String serviceProviderId, String where, int page, int limit)
    {
        List<VersionResource> resources = null;

        // Start of user code queryVersionResources
        try {
            URI versionResourceGraphName = ResourceVersionHandler.getVersionResourceGraphName(
                keyProvider.spUri(serviceProviderId));
            resources = store.getResources(versionResourceGraphName, VersionResource.class);
            resources = slicePaging(resources, page, limit);
        } catch (StoreAccessException | ModelUnmarshallingException e) {
            log.warn("Error while fetching VersionResources: {}", e);
        }
        // End of user code
        return resources;
    }


    public static VersionResource getVersionResource(HttpServletRequest httpServletRequest, final String serviceProviderId, final String resourceId, final String versionId)
    {
        VersionResource aResource = null;

        // Start of user code getVersionResource
        List<VersionResource> latestResources = queryVersionResources(httpServletRequest, serviceProviderId, null, -1,
                -1);

        Stream<VersionResource> resourceStream = latestResources.stream()
                .filter(vr -> resourceId.equals(vr.getIdentifier()) && versionId.equals(vr.getVersionId()));
        aResource = resourceStream.findFirst().orElse(null);
        // End of user code
        return aResource;
    }

The current integration was performed via the `ServiceProvidersFactory` class by injecting our Service:

    private static Class<?>[] RESOURCE_CLASSES =
    {
        RequirementAndRequirementCollectionService.class,
        VersionResourceService.class
    };

Basically `ServiceProviderFactory.initServiceProvider` does the `for (final Class<?> resourceClass : resourceClasses)` loop that allows us to inject our service for each and every Service Provider under another Service.