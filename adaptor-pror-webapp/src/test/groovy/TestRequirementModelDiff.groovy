import com.hp.hpl.jena.rdf.model.Model
import com.hp.hpl.jena.rdf.model.ModelFactory
import com.hp.hpl.jena.rdf.model.Statement
import groovy.transform.NotYetImplemented
import org.eclipse.lyo.oslc4j.provider.jena.JenaModelHelper
import org.junit.Test
import org.junit.runner.RunWith
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PowerMockIgnore
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import se.kth.md.assume.adaptors.pror.resources.Requirement
import se.kth.md.assume.adaptors.pror.servlet.ServletListener

import static org.hamcrest.Matchers.*
import static org.junit.Assert.assertThat
import static org.mockito.Mockito.when

/**
 * TestMain is .
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-06-21
 */
@RunWith(PowerMockRunner.class)
@PowerMockIgnore(["org.mockito.*", "org.apache.*", "javax.ws.*", "com.hp.hpl.jena.*", "org.slf4j.*"])
@PrepareForTest(ServletListener.class)
class TestRequirementModelDiff extends GroovyTestCase {
    /**
     * Number of statements in a model with two OSLC Requirement POJOs
     */
    public static final long JENA_DIFF_MODEL_STATEMENTS = 7L
    public static final String PROVIDER = "dummyProvider"

    def listA = []
    def listB = []
    def modelA
    def modelB

    /**
     * Dummy test if all others are disabled not to fail the build
     */
    void testPass() {}

    private void generateLists() {
        PowerMockito.mockStatic(ServletListener.class);
        when(ServletListener.getServicesBase()).thenReturn("test:///base/");

        def reqA = new Requirement(PROVIDER, "REQ-A", "Requirement A", "REQ-A description")
        def reqB = new Requirement(PROVIDER, "REQ-B", "Requirement B", "REQ-B description")
        def reqC = new Requirement(PROVIDER, "REQ-C", "Requirement C", "REQ-C description")
        def reqCPrime = new Requirement(PROVIDER, "REQ-C", "Requirement C", "Alt description")
        def reqD = new Requirement(PROVIDER, "REQ-D", "Requirement D", "REQ-D description")

        listA << reqA
        listA << reqB
        listA << reqC

        listB << reqB
        listB << reqCPrime
        listB << reqD
    }

    @Override
    void setUp() {
        generateLists()

        modelA = JenaModelHelper.createJenaModel(listA.toArray())
        modelB = JenaModelHelper.createJenaModel(listB.toArray())
    }

    /**
     * Test the intersections between A^B and B^A are exactly the same
     */
    @NotYetImplemented
    void testIntersectionDirect() {
        def modelIntersectionA = modelA.intersection(modelB)
        def modelIntersectionB = modelB.intersection(modelA)

        // TODO improve Jena Model.equals method
        assertEquals(modelIntersectionA, modelIntersectionB)
    }

    /**
     * Test the objects in the intersections between A^B and B^A are exactly the same
     */
    void testIntersectionTwoWay() {
        def modelIntersectionA = modelA.intersection(modelB)
        def modelIntersectionB = modelB.intersection(modelA)

        Object[] intersectionResourcesA = JenaModelHelper.fromJenaModel(modelIntersectionA, Requirement.class)
        Object[] intersectionResourcesB = JenaModelHelper.fromJenaModel(modelIntersectionB, Requirement.class)

        // requires Requirement.equals method
        assertThat(Arrays.asList(intersectionResourcesA), containsInAnyOrder(intersectionResourcesB))
    }

    /**
     * Check that intersections have equal number of statements (non-zero)
     */
    void testIntersectionEqualSize() {
        def modelIntersectionA = modelA.intersection(modelB)
        def modelIntersectionB = modelB.intersection(modelA)

        def intersectionResourcesA = JenaModelHelper.fromJenaModel(modelIntersectionA, Requirement.class)
        def intersectionResourcesB = JenaModelHelper.fromJenaModel(modelIntersectionB, Requirement.class)

        assertThat(modelIntersectionA.size(), equalTo(JENA_DIFF_MODEL_STATEMENTS))
        assertThat(intersectionResourcesA.size(), equalTo(2))
        assertEquals(intersectionResourcesA.size(), intersectionResourcesB.size())
    }

    /**
     * Test that an intersection with one changed field has one less Statement
     */
    void testIntersectionWithUpdates() {
        def listBPrime = listB.collect { it }
        listBPrime.set(0, new Requirement(PROVIDER, "REQ-B", "Requirement B", "Alt description"))
        def modelBPrime = JenaModelHelper.createJenaModel(listBPrime.toArray())
        def modelIntersectionA = modelA.intersection(modelBPrime)

        def intersectionResourcesA = JenaModelHelper.fromJenaModel(modelIntersectionA, Requirement.class)

        assertThat(modelIntersectionA.size(), lessThan(JENA_DIFF_MODEL_STATEMENTS))
        assertThat(modelIntersectionA.size(), greaterThan(0L))

        assertThat(intersectionResourcesA.size(), equalTo(2))  // may not always be the case
    }

    @Test(expected = NullPointerException)
    void testJenaRejectsNonOslcPojos() {
        def person = new Person("John", "Miller")
        def model = JenaModelHelper.createJenaModel(person)
        assertThat(model.listStatements().hasNext(), is(true))
    }

    void testJenaAllowsHandbuiltModels() {
        Model model = ModelFactory.createDefaultModel();
        def titleProperty = model.createProperty("oslc_codegen:", "repository_title")
        model.createResource("oslc://repos/new")
                .addProperty(titleProperty, "New Repository")


        Iterator<Statement> statements = model.listStatements()
        def listOfStatements = statements.toList()
        for (stmt in listOfStatements) {
            System.out.println(stmt)
        }
        assertThat(listOfStatements.size(), equalTo(1))
    }

    class Person {
        String name
        String surname

        Person(String name, String surname) {
            this.name = name
            this.surname = surname
        }

        String getName() {

            return name
        }

        void setName(String name) {
            this.name = name
        }

        String getSurname() {
            return surname
        }

        void setSurname(String surname) {
            this.surname = surname
        }
    }
}
