import com.google.common.collect.ImmutableSet
import com.google.common.collect.Sets

import static org.hamcrest.Matchers.not
import static org.hamcrest.Matchers.sameInstance
import static org.junit.Assert.assertThat

/**
 * TestGuavaSets is showing how we can get to different Set intersections if we game the equality check in our classes.
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-06-28
 */
class TestGuavaSets extends GroovyTestCase {

    void testIntersectionSourceIsCorrect() {
        def testObject = new String("John")
        def setA = ImmutableSet.of(testObject, "etc")
        def setB = ImmutableSet.of(new String("John"), "misc")

        def result = Sets.intersection(setA, setB)

        assertThat(result.getAt(0), sameInstance(testObject))
    }

    void testIntersectionSourceIsNotWrong() {
        def testObject = new String("John")
        def setA = ImmutableSet.of(testObject, "etc")
        def setB = ImmutableSet.of(new String("John"), "misc")

        def result = Sets.intersection(setB, setA)

        assertThat(result.getAt(0), not(sameInstance(testObject)))
    }

}
