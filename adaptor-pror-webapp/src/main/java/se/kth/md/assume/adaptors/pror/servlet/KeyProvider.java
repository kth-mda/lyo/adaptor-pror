package se.kth.md.assume.adaptors.pror.servlet;

import java.net.URI;

/**
 * Created on 27.02.17
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public interface KeyProvider {
  URI key(String id);
}
