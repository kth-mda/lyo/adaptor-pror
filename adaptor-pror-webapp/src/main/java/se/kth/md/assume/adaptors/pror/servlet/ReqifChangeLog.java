package se.kth.md.assume.adaptors.pror.servlet;

import java.net.URI;
import org.eclipse.lyo.tools.store.ModelUnmarshallingException;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.update.change.ChangeHelper;
import org.eclipse.lyo.tools.store.update.change.HistoryResource;
import se.kth.md.assume.adaptors.pror.ProRConstants;
import org.eclipse.lyo.oslc4j.trs.provider.ChangeHistories;
import org.eclipse.lyo.oslc4j.trs.provider.HistoryData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * ReqifChangeLog extends {@link ChangeHistories} in order to provide the
 * <a href="http://open-services.net/wiki/core/TrackedResourceSet-2.0/#Change-Log">OSLC TRS Change Log</a>.
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-06-21
 */
public class ReqifChangeLog extends ChangeHistories {

    private final static Logger LOGGER = LoggerFactory.getLogger(ReqifChangeLog.class);
    private final Store store;

    public ReqifChangeLog(long updateInterval, String serviceBase, Store store) {
        super(updateInterval, serviceBase);
        this.store = store;
    }

    @Override
    public HistoryData[] getHistory(HttpServletRequest httpServletRequest, Date dateAfter) {
        HistoryData[] historyData = new HistoryData[0];
        if (dateAfter == null) {
            dateAfter = defaultDateAfter();
        }
        LOGGER.info("Building TRS history since {}", dateAfter);
        try {
            List<HistoryData> historyDataList = fetchHistoryFromStore(dateAfter);
            historyData = historyDataList.toArray(new HistoryData[historyDataList.size()]);
            LOGGER.info("Returning TRS history since {}", dateAfter);
        } catch (StoreAccessException | ModelUnmarshallingException e) {
            LOGGER.error("Error retrieving TRS history from Store", e);
        }
        return historyData;
    }

    private List<HistoryData> fetchHistoryFromStore(Date dateAfter)
            throws StoreAccessException, ModelUnmarshallingException {
        // TODO: 2016-12-25 inject graph name
        List<HistoryResource> historyResources = store
                .getResources(URI.create(ProRConstants.TRS_GRAPH_NAME), HistoryResource.class);
        // TODO: 2016-12-25 return only data after the given date
        return ChangeHelper.mapFn(historyResources, ReqifChangeLog::buildHistoryData);
    }

    private Date defaultDateAfter() {
        Calendar cal = new GregorianCalendar(1970, Calendar.JANUARY, 1);
        return cal.getTime();
    }

    public static HistoryData buildHistoryData(HistoryResource r) {
        return HistoryData.getInstance(r.getTimestamp(), r.getResourceURI(), String.valueOf(r.getChangeKind()));
    }
}
