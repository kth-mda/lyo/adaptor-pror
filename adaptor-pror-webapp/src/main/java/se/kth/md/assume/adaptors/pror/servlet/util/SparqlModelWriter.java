package se.kth.md.assume.adaptors.pror.servlet.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import com.hp.hpl.jena.rdf.model.Model;

@Provider
@Produces("application/rdf+xml")
public class SparqlModelWriter implements MessageBodyWriter<Model> {

    ByteArrayOutputStream modelStream = null;

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(Model t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        writeStreamOnce(t);
        return modelStream.size();
    }

    private void writeStreamOnce(Model t) {
        if (modelStream == null) {
            modelStream = new ByteArrayOutputStream();
            t.write(modelStream, "RDF/XML-ABBREV");
        }

    }

    @Override
    public void writeTo(Model t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
            MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream)
                    throws IOException, WebApplicationException {
        writeStreamOnce(t);
        modelStream.writeTo(entityStream);
    }
}
