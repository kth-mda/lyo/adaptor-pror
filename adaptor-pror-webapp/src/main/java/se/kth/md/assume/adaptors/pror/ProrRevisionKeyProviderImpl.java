package se.kth.md.assume.adaptors.pror;

import java.net.URI;
import se.kth.md.aide.diff.RevisionKeyProviderImpl;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.assume.adaptors.pror.servlet.ServiceProviderCatalogSingleton;

/**
 * Created on 13.03.17
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public class ProrRevisionKeyProviderImpl extends RevisionKeyProviderImpl {

    @Override
    public URI serviceProviderUri(final String serviceProvider) {
        return ServiceProviderCatalogSingleton.constructServiceProviderURI(serviceProvider);
    }

    @Override
    public URI key(final String serviceProviderId, final Revision currentRevision) {
        URI serviceProviderURI = serviceProviderUri(serviceProviderId);
        URI uri = URI.create(serviceProviderURI.toString() + '/' + currentRevision.getId());
        return uri;
    }

    @Deprecated
    public URI spUri(final String serviceProviderIdFor) {
        return serviceProviderUri(serviceProviderIdFor);
    }
}
