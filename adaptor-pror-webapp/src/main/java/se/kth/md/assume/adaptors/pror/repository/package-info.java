/**
 * Adaptor-specific classes for use with the {@link se.kth.md.assume.repository} package.
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
package se.kth.md.assume.adaptors.pror.repository;
