package se.kth.md.assume.adaptors.pror.repository;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.diff.RepositoryResourceExtractor;
import se.kth.md.aide.repository.api.Filter;
import se.kth.md.aide.repository.api.FilterAbstractFactory;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.RevisionAbstractObject;
import se.kth.md.assume.adaptors.pror.resources.Requirement;
import se.kth.mmk.md.ReqIfResource;
import se.kth.mmk.md.ReqifSpecification;
import se.kth.mmk.md.ReqifSpecificationElement;

import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * RequirementExtractor is .
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-06-13
 */
public class RequirementExtractor implements RepositoryResourceExtractor {
    private static final String REFINEMENT_LABEL = "refinement";
    private static final String REQIF_EXTENSION = ".reqif";

    private final Logger log = LoggerFactory.getLogger(RequirementExtractor.class);

    @Override
    public String getFileSuffix() {
        return RequirementExtractor.REQIF_EXTENSION;
    }

    @Override
    public Filter getFilter(final FilterAbstractFactory filterFactory) {
        return filterFactory.getPathFilter(RequirementExtractor.REQIF_EXTENSION);
    }

    @Override
    public List<AbstractResource> extractFileRevisionResources(RevisionAbstractObject revisionedObject, Repository repository) {
        List<AbstractResource> resources = new ArrayList<>();
        InputStream objectStream = null;
        try {
            objectStream = repository.getObjectStream(revisionedObject);
        } catch (IOException e) {
            log.error("Failed to read the InputStream of {}", revisionedObject);
            // TODO /Andrew 13.03.17: use checked exception
            throw new IllegalStateException(e);
        }
        ReqIfResource resource = new ReqIfResource(objectStream);

        List<ReqifSpecification> specifications = resource.getSpecifications();
        ReqifSpecification firstReqifSpecification = specifications.get(0);
        try {
            buildRequirementsFromSpec(resources, firstReqifSpecification.getSpecificationElements(), null,
                    revisionedObject.getPath());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return resources;
    }

    /**
     * Build a {@link Requirement} from a corresponding {@link ReqifSpecificationElement}.
     *
     * @param serviceProviderId {@link ServiceProvider} ID to generate absolute {@link Requirement} URIs.
     * @param requirement       Source ReqIf {@link ReqifSpecificationElement}.
     * @return {@link Requirement} from the OSLC RM domain.
     * @throws URISyntaxException
     */
    private Requirement buildRequirement(String serviceProviderId, ReqifSpecificationElement requirement)
            throws URISyntaxException {
        String requirementId = getAttributeStringOrDefault(requirement, "ID", requirement.getIdentifier());
        String title = getAttributeStringOrDefault(requirement, "Title", "Title missing");
        String description = getAttributeStringOrDefault(requirement, "Description", null);
        Requirement newRequirement = new Requirement(serviceProviderId, requirementId, title, description);

        String documentation = getAttributeStringOrDefault(requirement, "Linked Data External Reference", null);
        if (documentation != null) {
            HashSet<Link> documentationLinks = new HashSet<>();
            documentationLinks.add(new Link(URI.create(documentation)));
            newRequirement.setDocumentation(documentationLinks);
        }

        return newRequirement;
    }

    private String getAttributeStringOrDefault(ReqifSpecificationElement requirement, String name, String alt) {
        String requirementIdentifier = alt;
        try {
            String idAttribute = requirement.getAttribute(name).getStringValue();
            if (StringUtils.isNotBlank(idAttribute)) {
                requirementIdentifier = idAttribute;
            }
        } catch (Exception e) {
            log.debug("Attribute '{}' is missing in {}", name, requirement);
        }
        return requirementIdentifier;
    }

    /**
     * Recursively build {@link Requirement} from {@link ReqifSpecificationElement} hierarchy.
     *
     * @param resources                     List to append built {@link Requirement} instances
     * @param reqifSpecificationElementList {@link ReqifSpecificationElement} hierarchy
     * @param parent                        Parent {@link Requirement} from {@link ReqifSpecificationElement}
     *                                      hierarchy, unless root.
     * @param serviceProviderId             {@link ServiceProvider#identifier} from the request arguments.
     * @throws URISyntaxException in case {@link Requirement} URI can't be resolved.
     */
    private void buildRequirementsFromSpec(List<AbstractResource> resources,
            List<ReqifSpecificationElement> reqifSpecificationElementList, Requirement parent, String serviceProviderId)
            throws URISyntaxException {
        for (ReqifSpecificationElement requirement : reqifSpecificationElementList) {
            final Requirement oslcRequirement = buildRequirement(serviceProviderId, requirement);
            if (parent != null) {
                // assume the graph is acyclic, will need to change approach for free-form ReqIf links
                oslcRequirement.addElaborates(new Link(parent.getAbout(), REFINEMENT_LABEL));
                parent.addElaborated_by(new Link(oslcRequirement.getAbout(), REFINEMENT_LABEL));
            }
            resources.add(oslcRequirement);
            final List<ReqifSpecificationElement> children = requirement.getChildren();
            if (children != null && children.size() > 0) {
                buildRequirementsFromSpec(resources, children, oslcRequirement, serviceProviderId);
            }
        }
    }
}
