package se.kth.md.assume.adaptors.pror.services;

import org.eclipse.lyo.oslc4j.trs.provider.ChangeHistories;
import org.eclipse.lyo.oslc4j.trs.provider.service.TrackedResourceSetService;
import se.kth.md.assume.adaptors.pror.ProRManager;
import se.kth.md.assume.adaptors.pror.servlet.ReqifChangeLog;
import se.kth.md.assume.adaptors.pror.servlet.ServletListener;

/**
 * ReqifTrsService handles TRS HTTP requests.
 *
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-06-21
 */
public class ReqifTrsService extends TrackedResourceSetService {

	private static final int UPDATE_INTERVAL = seconds(120);

	private final ChangeHistories changeLog = new ReqifChangeLog(UPDATE_INTERVAL, getServiceBase(), ProRManager.getStore());

	private static int seconds(int s) {
		return s;
	}

	@Override
	protected ChangeHistories getChangeHistories() {
		return changeLog;
	}

	@Override
	protected String getServiceBase() {
		return ServletListener.getServicesBase();
	}
}
