ProR OSLC adapter
================================

Mostly, it's an automatically generated code using Eclipse
Lyo Modeling tools (pre-release version).

In order to start working on it, run

    mvn clean test package jetty:run-exploded

It will run cleanup, tests, compilation & packaging, and,
finally, a Jetty server.

Go to the [preview
page](http://127.0.0.1:8080/codeGenProR/services/rm-services/serviceProvider/resources/requirements)
in order to see the summary of all `Requirement` resources.

## Credentials

You need two pairs of credentials:

1. AWS keys for S3 access
2. Tomcat credentials with `manager-script` role.

### Maven setup

Add the following into `~/.m2/settings.xml` where `~` is you home user directory.

    <settings>
      <servers>
        <server>
          <id>kth-releases</id>
          <username>AWS_ACCESS_KEY</username>
          <password>AWS_SECRET_KEY</password>
        </server>
        <server>
          <id>kth-snapshots</id>
          <username>AWS_ACCESS_KEY</username>
          <password>AWS_SECRET_KEY</password>
        </server>
        <server>
          <id>kth-tomcat</id>
          <username>maven</username><!-- Or another username -->
          <password>TOMCAT_PASSWORD</password>
        </server>
      </servers>
    </settings>

### Gradle setup

Add the following section to `~/.gradle/gradle.properties`,
where `~` is you home user directory.

    # Auth

    awsKey = AWS_ACCESS_KEY
    awsSecret = AWS_SECRET_KEY

Note the lack of quotes.

## Configuration

You must adjust the context parameter
`se.kth.md.assume.adaptors.pror.servlet.repository.dir` in the `web.xml`. The folder must point to a proper git repository
with at least one commit.

By default, Store is configured to use in-memory Jena TDB.

## Local testing

### Maven

    mvn clean test package jetty:run-exploded

### Gradle

    ./gradlew clean test assemble jettyRun

## Deployment

    mvn tomcat7:redeploy

Should give you the following output:

    [INFO] --- tomcat7-maven-plugin:2.2:redeploy (default-cli) @ codeGenProR ---
    [INFO] Deploying war to http://git.md.kth.se:8081/pror
    Uploading: http://git.md.kth.se:8081/manager/text/deploy?path=%2Fpror&update=true
    Uploaded: http://git.md.kth.se:8081/manager/text/deploy?path=%2Fpror&update=true (18061 KB at 1289.9 KB/sec)

    [INFO] tomcatManager status code:200, ReasonPhrase:OK
    [INFO] OK - Undeployed application at context path /pror
    [INFO] OK - Deployed application at context path /pror
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESS
    [INFO] ------------------------------------------------------------------------
